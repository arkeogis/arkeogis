#!/bin/bash

me=$(whoami)

cat sql/*.sql > /tmp/arkeo-import.sql
chmod +r /tmp/arkeo-import.sql

sudo bash <<EOF
echo -n "import..."
cd /tmp
su postgres -c "psql arkeogis < /tmp/arkeo-import.sql"
EOF
rm -f /tmp/arkeo-import.sql
echo " done."
