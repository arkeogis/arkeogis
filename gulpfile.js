var gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
var minifyCss = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var shell = require('gulp-shell');
var child = require('child_process');
var util = require('gulp-util');
var reload = require('gulp-livereload');

var server = null;

var gobuild = function(srcpath, outfile, cb) {
    var build = child.spawn('go',
                            ['build', '-o', outfile, srcpath],
                            { cwd: __dirname+'/src/server' });
    build.stdout.on('data', function(data) {
        util.log(util.colors.green(
            'go build : ' + data
        ));
    });
    build.stderr.on('data', function(data) {
        util.log(util.colors.red(
            'go build : ' + data
        ));
    });
    build.on('close', function(code) {
        util.log(util.colors.green(
            'go build done : ' + code
        ));
        cb();
    });
}

var paths = {
  sass: [__dirname+'/src/web/scss/**/*.scss'],
	app: [

			__dirname+'/src/web/bower_components/jquery/dist/jquery.min.js',
			__dirname+'/src/web/bower_components/angular/angular.min.js',
			__dirname+'/src/web/bower_components/angular-i18n/angular-locale_fr.js',
			__dirname+'/src/web/bower_components/angular-i18n/angular-locale_de.js',
			__dirname+'/src/web/bower_components/angular-i18n/angular-locale_en.js',
			__dirname+'/src/web/bower_components/angular-i18n/angular-locale_es.js',
			__dirname+'/src/web/bower_components/angular-cookies/angular-cookies.min.js',
			__dirname+'/src/web/bower_components/angular-messages/angular-messages.min.js',
			__dirname+'/src/web/bower_components/angular-ui-router/release/angular-ui-router.min.js',
			__dirname+'/src/web/bower_components/angular-resource/angular-resource.min.js',
			__dirname+'/src/web/bower_components/angular-animate/angular-animate.min.js',
			__dirname+'/src/web/bower_components/angular-aria/angular-aria.min.js',
			__dirname+'/src/web/bower_components/angular-material/angular-material.min.js',
			__dirname+'/src/web/bower_components/ui-leaflet/dist/ui-leaflet.min.js',
			__dirname+'/src/web/bower_components/angular-simple-logger/dist/angular-simple-logger.min.js',
			__dirname+'/src/web/bower_components/angular-promise-extras/angular-promise-extras.js',
			__dirname+'/src/web/bower_components/ng-idle/angular-idle.min.js',
			__dirname+'/src/web/bower_components/leaflet/dist/leaflet.js',
			__dirname+'/src/web/bower_components/leaflet.markercluster/dist/leaflet.markercluster.js',
			__dirname+'/src/web/bower_components/leaflet-plugins/layer/tile/Google.js',
			__dirname+'/src/web/bower_components/leaflet-draw/dist/leaflet.draw.js',
			__dirname+'/src/web/bower_components/angular-translate/angular-translate.min.js',
			__dirname+'/src/web/bower_components/ng-file-upload/ng-file-upload-shim.min.js',
			__dirname+'/src/web/bower_components/ng-file-upload/ng-file-upload.min.js',
			__dirname+'/src/web/bower_components/x2js/xml2json.min.js',
			__dirname+'/src/web/bower_components/angular-xml/angular-xml.min.js',
			__dirname+'/src/web/bower_components/shp/dist/shp.min.js',
			__dirname+'/src/web/bower_components/lodash/dist/lodash.min.js',
			__dirname+'/src/web/bower_components/angular-material-data-table/dist/md-data-table.min.js',
			__dirname+'/src/web/bower_components/d3/d3.min.js',
			__dirname+'/src/web/bower_components/nvd3/build/nv.d3.min.js',
			__dirname+'/src/web/bower_components/angular-nvd3/dist/angular-nvd3.min.js',
			__dirname+'/src/web/bower_components/angular-translate-loader-static-files/angular-translate-loader-static-files.min.js',
			__dirname+'/src/web/ext/leaflet.TileLayer.WMTS/leaflet-tilelayer-wmts-src-croll.js',
			__dirname+'/src/web/ext/leaflet.arkeo.controls.js',
			__dirname+'/src/web/ext/Leaflet.StyledLayerControl/src/styledLayerControl.js',
			__dirname+'/src/web/ext/L.Control.MousePosition.js',
      __dirname+'/src/web/ext/leaflet-GIBS/src/GIBSMetadata.js',
      __dirname+'/src/web/ext/leaflet-GIBS/src/GIBSLayer.js',
		  __dirname+'/src/web/app/app.js',
          __dirname+'/src/web/app/services/**/*.js',
          __dirname+'/src/web/app/controllers/**/*.js',
		  __dirname+'/src/web/app/filters.js',
          __dirname+'/src/web/app/directives/**/*.js'],
	web: [__dirname+'/src/web/index.html',
          __dirname+'/src/web/img/**/*',
          __dirname+'/src/web/ext/**/*',
          __dirname+'/src/web/material-design-icons/**/*',
          __dirname+'/src/web/partials/**/*.html',
          __dirname+'/src/web/languages/*.json',
         ],
	go: [__dirname+'/src/server/**/*.go'],
    serverlangs: [__dirname+'/src/server/languages/*.json'],
    //serverfiles: [__dirname+'/src/server/config.json'],
    serverfiles: [],
	bower_components: [__dirname+'/src/web/bower_components/**/*']
};

gulp.task('web:sass', function() {
    return gulp.src(__dirname+'/src/web/scss/*.scss')
        .pipe(sass({
            errLogToConsole: true
        }))
        .pipe(gulp.dest(__dirname+'/dist/public/css/'))
	.pipe(reload());
});

gulp.task('web:sass-min', function() {
    return gulp.src(__dirname+'/src/web/scss/*.scss')
        .pipe(sass({
            errLogToConsole: true
        }))
        .pipe(minifyCss({
            keepSpecialComments: 0
        }))
        .pipe(gulp.dest(__dirname+'/dist/public/css/'))
	.pipe(reload());
});

gulp.task('web:app', function() {
    return gulp.src(paths.app, {base: __dirname+'/src/web/'})
        .pipe(gulp.dest(__dirname+'/dist/public'))
	.pipe(reload());
});

gulp.task('web:bower_components', function() {
    return gulp.src(paths.bower_components, {base: __dirname+'/src/web/'})
        .pipe(gulp.dest(__dirname+'/dist/public'))
	.pipe(reload());
});

gulp.task('web:app-min', function() {
    return gulp.src(paths.app, {base: __dirname+'/src/web/'})
	.pipe(sourcemaps.init())
     //   .pipe(uglify())
	.pipe(concat('arkeo.all.min.js'))
	.pipe(sourcemaps.write())
        .pipe(gulp.dest(__dirname+'/dist/public/app'))
	.pipe(reload());
});

//gulp.task('web:copy', ['web:app-min'], function() {
gulp.task('web:copy', function() {
    return gulp.src(paths.web, {base: __dirname+'/src/web/'})
	.pipe(gulp.dest(__dirname+'/dist/public'))
	.pipe(reload());
});

gulp.task('server:copy', function() {
    return gulp.src(paths.serverfiles, {base: __dirname+'/src/server/'})
	.pipe(gulp.dest(__dirname+'/dist'))
	.pipe(reload());
});

gulp.task('serverlangs:copy', function() {
    return gulp.src(paths.serverlangs, {base: __dirname+'/src/server/'})
	.pipe(gulp.dest(__dirname+'/dist'));
});

gulp.task('server:build', [], function(cb) {
    gobuild("main.go", '../../dist/arkeo-server', cb);
});

gulp.task('tools-transparse:build', [], function(cb) {
    gobuild("src/gitlab.huma-num.fr/arkeogis/arkeogis-server/tools/transparse/transparse.go", '../../dist/tools/transparse', cb);
});

gulp.task('tools-xmltopsql:build', [], function(cb) {
    gobuild("src/gitlab.huma-num.fr/arkeogis/arkeogis-server/tools/xmltopsql/xmltopsql.go", '../../dist/tools/xmltopsql', cb);
});

gulp.task('tools-xmltogo:build', [], function(cb) {
    gobuild("src/gitlab.huma-num.fr/arkeogis/arkeogis-server/tools/xmltogo/xmltogo.go", '../../dist/tools/xmltogo', cb);
});

gulp.task('tools-geonames_import:build', [], function(cb) {
    gobuild("src/gitlab.huma-num.fr/arkeogis/arkeogis-server/tools/geonames/geonames_import.go", '../../dist/tools/geonames_import', cb);
});

gulp.task('tools-caracsimport:build', [], function(cb) {
    gobuild("src/gitlab.huma-num.fr/arkeogis/arkeogis-server/tools/caracsimport/caracsimport.go", '../../dist/tools/caracsimport', cb);
});

gulp.task('tools-restdoc:build', [], function(cb) {
    //gobuild("src/gitlab.huma-num.fr/arkeogis/arkeogis-server/tools/restdoc/restdoc.go", '../../dist/tools/restdoc', cb);
});

// tools:build will build all tools
gulp.task('tools:build', ['tools-transparse:build', 'tools-xmltopsql:build', 'tools-xmltogo:build', 'tools-geonames_import:build', 'tools-caracsimport:build', 'tools-restdoc:build']);


gulp.task('server:run', ['server:build', 'server:copy'], function() {

    if (server)
        server.kill();

    server = child.spawn('./arkeo-server', [
        "dev"
    ], {
        cwd: __dirname+'/dist',
    });

    server.stdout.once('data', function() {
        reload.reload('/');
    });

    server.stdout.on('data', function(data) {
        var lines = data.toString().split('\n')
        for (var l in lines)
            if (lines[l].length)
                util.log(lines[l]);
    });

    server.stderr.on('data', function(data) {
        process.stdout.write(data.toString());
    });

    return server;
});

gulp.task('server:debug', ['server:build', 'server:copy'], function() {

    if (server)
        server.kill();

    server = child.spawn('dlv debug arkeo-server', [
        "dev"
    ], {
        cwd: __dirname+'/dist',
    });

    server.stdout.once('data', function() {
        reload.reload('/');
    });

	/*
    server.stdout.on('data', function(data) {
        var lines = data.toString().split('\n')
        for (var l in lines)
            if (lines[l].length)
                util.log(lines[l]);
    });
	*/

    server.stderr.on('data', function(data) {
        process.stdout.write(data.toString());
    });

    return server;
});

gulp.task('server:stop', function(cb) {
    if (server) {
        setTimeout(function () {
            server.kill();
            cb();
        }, 1000);
    }
});

// this task is used when working on code, it will recompile/copy/etc code immediatly when saving a file
gulp.task('watch', function() {
    reload.listen();
    gulp.watch(paths.sass, ['web:sass']);
    gulp.watch(paths.app, ['web:app', 'web:app-min']);
    gulp.watch(paths.web, ['web:copy']);
    gulp.watch(paths.serverlangs, ['serverlangs:copy']);
    gulp.watch(paths.serverfiles, ['server:copy', 'server:stop', 'server:build', 'server:run']);
    //gulp.watch(paths.serverfiles, ['server:copy', 'server:stop', 'server:build', 'tools:build', 'server:run']);
    //gulp.watch(paths.bower_components, ['web:bower_components']);
    //gulp.watch(paths.go, ['server:stop', 'server:build', 'tools:build', 'server:run']);
    gulp.watch(paths.go, ['server:stop', 'server:build', 'server:run']);
});

// this will build all tools
gulp.task('tools', ['tools:build']);

// this will build all for release, public code will be minifyed for performance
gulp.task('release', ['web:sass-min', 'web:app-min', 'web:copy', 'web:bower_components', 'server:copy', 'server:build']);

// default action will run server without minifying public code
//gulp.task('default', ['watch', 'web:sass', 'web:app', 'web:copy', 'web:bower_components', 'server:run']);
gulp.task('default', ['watch', 'web:sass', 'web:sass-min', 'web:app', 'web:app-min', 'web:copy', 'server:run']);

gulp.task('debug', ['watch', 'web:sass', 'web:app', 'web:copy', 'web:bower_components', 'server:debug']);

gulp.task('release-web', ['web:sass-min', 'web:app-min', 'web:copy', 'web:bower_components']);

gulp.task('dev-web', ['watch', 'web:sass', 'web:app', 'web:copy', 'web:bower_components', 'server:run']);
