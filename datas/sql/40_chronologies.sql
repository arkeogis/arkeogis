--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Data for Name: chronology; Type: TABLE DATA; Schema: public; Owner: arkeogis
--

COPY chronology (id, parent_id, start_date, end_date, color, created_at, updated_at) FROM stdin;
0	0	0	0		2016-06-21 19:48:23.222022	2016-06-21 19:48:23.222022
227	0	-724	486		2016-06-24 16:36:11.204316	2016-06-24 18:46:32.00249
237	227	-117	486	16aaef	2016-06-24 16:36:11.204316	2016-06-24 18:46:32.00249
274	0	-699	-75		2016-06-25 15:57:33.046991	2016-06-25 16:01:34.624429
261	0	-13999	-4500		2016-06-25 07:59:55.183295	2016-06-25 15:48:46.104829
262	261	-13999	-10200	5882ff	2016-06-25 08:02:26.093256	2016-06-25 15:48:46.104829
264	262	-13999	-12000	7294f6	2016-06-25 08:09:57.643595	2016-06-25 15:48:46.104829
265	262	-11999	-10200	7294f6	2016-06-25 08:09:57.643595	2016-06-25 15:48:46.104829
263	261	-10199	-4500	16aaef	2016-06-25 08:02:26.093256	2016-06-25 15:48:46.104829
266	263	-10199	-8800	4fbcf3	2016-06-25 15:44:43.767416	2016-06-25 15:48:46.104829
267	263	-8799	-7600	4fbcf3	2016-06-25 15:44:43.767416	2016-06-25 15:48:46.104829
228	227	-724	-118	5882ff	2016-06-24 16:36:11.204316	2016-06-24 18:46:32.00249
229	228	-724	-675	7294f6	2016-06-24 16:36:11.204316	2016-06-24 18:46:32.00249
230	228	-674	-525	7294f6	2016-06-24 16:36:11.204316	2016-06-24 18:46:32.00249
231	230	-674	-625	85a0f5	2016-06-24 16:36:11.204316	2016-06-24 18:46:32.00249
232	230	-624	-525	85a0f5	2016-06-24 16:36:11.204316	2016-06-24 18:46:32.00249
233	228	-524	-425	7294f6	2016-06-24 16:36:11.204316	2016-06-24 18:46:32.00249
234	228	-424	-118	7294f6	2016-06-24 16:36:11.204316	2016-06-24 18:46:32.00249
235	234	-424	-300	85a0f5	2016-06-24 16:36:11.204316	2016-06-24 18:46:32.00249
236	234	-299	-118	85a0f5	2016-06-24 16:36:11.204316	2016-06-24 18:46:32.00249
268	263	-7599	-6900	4fbcf3	2016-06-25 15:44:43.767416	2016-06-25 15:48:46.104829
269	263	-6899	-6400	4fbcf3	2016-06-25 15:44:43.767416	2016-06-25 15:48:46.104829
270	263	-6399	-5800	4fbcf3	2016-06-25 15:44:43.767416	2016-06-25 15:48:46.104829
271	263	-5799	-5400	4fbcf3	2016-06-25 15:44:43.767416	2016-06-25 15:48:46.104829
272	263	-5399	-5000	4fbcf3	2016-06-25 15:44:43.767416	2016-06-25 15:48:46.104829
273	263	-4999	-4500	4fbcf3	2016-06-25 15:44:43.767416	2016-06-25 15:48:46.104829
275	274	-699	-75	5882ff	2016-06-25 16:01:34.624429	2016-06-25 16:01:34.624429
238	229	-724	-700	85a0f5	2016-06-24 16:52:23.844922	2016-06-24 18:46:32.00249
239	229	-699	-675	85a0f5	2016-06-24 16:52:23.844922	2016-06-24 18:46:32.00249
240	231	-674	-650	a2b6ef	2016-06-24 16:52:23.844922	2016-06-24 18:46:32.00249
241	231	-649	-625	a2b6ef	2016-06-24 16:52:23.844922	2016-06-24 18:46:32.00249
242	232	-624	-600	a2b6ef	2016-06-24 16:52:23.844922	2016-06-24 18:46:32.00249
243	232	-599	-575	a2b6ef	2016-06-24 16:52:23.844922	2016-06-24 18:46:32.00249
244	232	-574	-550	a2b6ef	2016-06-24 16:52:23.844922	2016-06-24 18:46:32.00249
245	232	-549	-525	a2b6ef	2016-06-24 16:52:23.844922	2016-06-24 18:46:32.00249
246	233	-524	-500	85a0f5	2016-06-24 16:52:23.844922	2016-06-24 18:46:32.00249
247	233	-499	-475	85a0f5	2016-06-24 16:52:23.844922	2016-06-24 18:46:32.00249
248	233	-474	-450	85a0f5	2016-06-24 16:52:23.844922	2016-06-24 18:46:32.00249
249	233	-449	-425	85a0f5	2016-06-24 16:52:23.844922	2016-06-24 18:46:32.00249
250	235	-424	-400	a2b6ef	2016-06-24 16:52:23.844922	2016-06-24 18:46:32.00249
251	235	-399	-375	a2b6ef	2016-06-24 16:52:23.844922	2016-06-24 18:46:32.00249
252	235	-374	-350	a2b6ef	2016-06-24 16:52:23.844922	2016-06-24 18:46:32.00249
253	235	-349	-325	a2b6ef	2016-06-24 16:52:23.844922	2016-06-24 18:46:32.00249
254	235	-324	-300	a2b6ef	2016-06-24 16:52:23.844922	2016-06-24 18:46:32.00249
255	236	-299	-275	a2b6ef	2016-06-24 16:52:23.844922	2016-06-24 18:46:32.00249
256	236	-274	-250	a2b6ef	2016-06-24 16:52:23.844922	2016-06-24 18:46:32.00249
257	236	-249	-225	a2b6ef	2016-06-24 16:52:23.844922	2016-06-24 18:46:32.00249
258	236	-224	-200	a2b6ef	2016-06-24 16:52:23.844922	2016-06-24 18:46:32.00249
259	236	-199	-150	a2b6ef	2016-06-24 16:52:23.844922	2016-06-24 18:46:32.00249
260	236	-149	-118	a2b6ef	2016-06-24 16:52:23.844922	2016-06-24 18:46:32.00249
41	37	-24	449	32ad5d	2016-06-22 18:36:35.780087	2016-06-24 13:33:12.506066
97	41	-24	36	4dc276	2016-06-23 07:28:51.779519	2016-06-24 13:33:12.506066
106	97	-24	13	6edb94	2016-06-23 07:54:25.931737	2016-06-24 13:33:12.506066
66	60	-1399	-1350	8ad6fc	2016-06-22 20:21:13.814121	2016-06-24 13:33:12.506066
48	39	-1349	-800	4fbcf3	2016-06-22 18:43:43.225924	2016-06-24 13:33:12.506066
69	48	-1349	-1050	6fc9f6	2016-06-22 20:26:09.07081	2016-06-24 13:33:12.506066
70	69	-1349	-1250	8ad6fc	2016-06-22 20:33:16.478127	2016-06-24 13:33:12.506066
71	69	-1249	-1050	8ad6fc	2016-06-22 20:33:16.478127	2016-06-24 13:33:12.506066
68	48	-1049	-900	6fc9f6	2016-06-22 20:26:09.07081	2016-06-24 13:33:12.506066
72	68	-1049	-1000	8ad6fc	2016-06-22 20:33:16.478127	2016-06-24 13:33:12.506066
73	68	-999	-900	8ad6fc	2016-06-22 20:33:16.478127	2016-06-24 13:33:12.506066
67	48	-899	-800	6fc9f6	2016-06-22 20:26:09.07081	2016-06-24 13:33:12.506066
74	67	-899	-850	8ad6fc	2016-06-22 20:33:16.478127	2016-06-24 13:33:12.506066
75	67	-849	-800	8ad6fc	2016-06-22 20:33:16.478127	2016-06-24 13:33:12.506066
40	37	-799	-25	01b5b5	2016-06-22 18:36:35.780087	2016-06-24 13:33:12.506066
76	40	-799	-620	18d5d2	2016-06-23 07:03:40.634979	2016-06-24 13:33:12.506066
82	76	-799	-725	3ae7e6	2016-06-23 07:10:00.665988	2016-06-24 13:33:12.506066
87	82	-799	-750	95eded	2016-06-23 07:18:41.743003	2016-06-24 13:33:12.506066
88	82	-749	-725	95eded	2016-06-23 07:18:41.743003	2016-06-24 13:33:12.506066
83	76	-724	-620	3ae7e6	2016-06-23 07:10:00.665988	2016-06-24 13:33:12.506066
146	83	-724	-700	95eded	2016-06-23 18:28:44.020859	2016-06-24 13:33:12.506066
147	83	-699	-620	95eded	2016-06-23 18:28:44.020859	2016-06-24 13:33:12.506066
77	40	-619	-460	18d5d2	2016-06-23 07:03:40.634979	2016-06-24 13:33:12.506066
84	77	-619	-530	3ae7e6	2016-06-23 07:10:00.665988	2016-06-24 13:33:12.506066
148	84	-619	-575	95eded	2016-06-24 06:43:33.940923	2016-06-24 13:33:12.506066
149	84	-574	-530	95eded	2016-06-24 06:43:33.940923	2016-06-24 13:33:12.506066
85	77	-529	-500	3ae7e6	2016-06-23 07:10:00.665988	2016-06-24 13:33:12.506066
150	85	-529	-525	95eded	2016-06-24 06:43:33.940923	2016-06-24 13:33:12.506066
151	85	-524	-500	95eded	2016-06-24 06:43:33.940923	2016-06-24 13:33:12.506066
86	77	-499	-460	3ae7e6	2016-06-23 07:10:00.665988	2016-06-24 13:33:12.506066
152	86	-499	-475	95eded	2016-06-24 06:43:33.940923	2016-06-24 13:33:12.506066
153	86	-474	-460	95eded	2016-06-24 06:43:33.940923	2016-06-24 13:33:12.506066
78	40	-459	-400	18d5d2	2016-06-23 07:03:40.634979	2016-06-24 13:33:12.506066
89	78	-459	-430	3ae7e6	2016-06-23 07:18:41.743003	2016-06-24 13:33:12.506066
154	89	-459	-445	95eded	2016-06-24 11:41:07.843039	2016-06-24 13:33:12.506066
155	89	-444	-430	95eded	2016-06-24 11:41:07.843039	2016-06-24 13:33:12.506066
90	78	-429	-400	3ae7e6	2016-06-23 07:18:41.743003	2016-06-24 13:33:12.506066
156	90	-429	-415	95eded	2016-06-24 11:43:38.165815	2016-06-24 13:33:12.506066
157	90	-414	-400	95eded	2016-06-24 11:43:38.165815	2016-06-24 13:33:12.506066
79	40	-399	-260	18d5d2	2016-06-23 07:03:40.634979	2016-06-24 13:33:12.506066
91	79	-399	-320	3ae7e6	2016-06-23 07:18:41.743003	2016-06-24 13:33:12.506066
158	91	-399	-350	95eded	2016-06-24 11:43:38.165815	2016-06-24 13:33:12.506066
159	91	-349	-320	95eded	2016-06-24 11:43:38.165815	2016-06-24 13:33:12.506066
92	79	-319	-260	3ae7e6	2016-06-23 07:18:41.743003	2016-06-24 13:33:12.506066
217	92	-319	-275	95eded	2016-06-24 13:27:55.486093	2016-06-24 13:33:12.506066
218	92	-274	-260	95eded	2016-06-24 13:27:55.486093	2016-06-24 13:33:12.506066
80	40	-259	-150	18d5d2	2016-06-23 07:03:40.634979	2016-06-24 13:33:12.506066
93	80	-259	-200	3ae7e6	2016-06-23 07:18:41.743003	2016-06-24 13:33:12.506066
219	93	-259	-225	95eded	2016-06-24 13:27:55.486093	2016-06-24 13:33:12.506066
220	93	-224	-200	95eded	2016-06-24 13:27:55.486093	2016-06-24 13:33:12.506066
94	80	-199	-150	3ae7e6	2016-06-23 07:18:41.743003	2016-06-24 13:33:12.506066
221	94	-199	-175	95eded	2016-06-24 13:30:42.609587	2016-06-24 13:33:12.506066
222	94	-174	-150	95eded	2016-06-24 13:30:42.609587	2016-06-24 13:33:12.506066
81	40	-149	-25	18d5d2	2016-06-23 07:03:40.634979	2016-06-24 13:33:12.506066
95	81	-149	-75	3ae7e6	2016-06-23 07:18:41.743003	2016-06-24 13:33:12.506066
223	95	-149	-125	95eded	2016-06-24 13:30:42.609587	2016-06-24 13:33:12.506066
224	95	-124	-75	95eded	2016-06-24 13:30:42.609587	2016-06-24 13:33:12.506066
96	81	-74	-25	3ae7e6	2016-06-23 07:18:41.743003	2016-06-24 13:33:12.506066
225	96	-74	-50	95eded	2016-06-24 13:30:42.609587	2016-06-24 13:33:12.506066
226	96	-49	-25	95eded	2016-06-24 13:30:42.609587	2016-06-24 13:33:12.506066
38	37	-6499	-2200	5882ff	2016-06-22 18:04:27.50741	2016-06-24 13:33:12.506066
39	37	-2199	-800	16aaef	2016-06-22 18:28:47.921258	2016-06-24 13:33:12.506066
46	39	-2199	-1600	4fbcf3	2016-06-22 18:43:43.225924	2016-06-24 13:33:12.506066
49	46	-2199	-1800	6fc9f6	2016-06-22 18:50:14.057565	2016-06-24 13:33:12.506066
52	49	-2199	-2000	8ad6fc	2016-06-22 18:51:55.380843	2016-06-24 13:33:12.506066
53	49	-1999	-1800	8ad6fc	2016-06-22 18:51:55.380843	2016-06-24 13:33:12.506066
50	46	-1799	-1650	6fc9f6	2016-06-22 18:50:14.057565	2016-06-24 13:33:12.506066
54	50	-1799	-1700	8ad6fc	2016-06-22 18:54:14.418885	2016-06-24 13:33:12.506066
55	50	-1699	-1650	8ad6fc	2016-06-22 18:54:14.418885	2016-06-24 13:33:12.506066
51	46	-1649	-1600	6fc9f6	2016-06-22 18:50:14.057565	2016-06-24 13:33:12.506066
56	51	-1649	-1625	8ad6fc	2016-06-22 18:54:14.418885	2016-06-24 13:33:12.506066
57	51	-1624	-1600	8ad6fc	2016-06-22 18:54:14.418885	2016-06-24 13:33:12.506066
47	39	-1599	-1350	4fbcf3	2016-06-22 18:43:43.225924	2016-06-24 13:33:12.506066
58	47	-1599	-1475	6fc9f6	2016-06-22 18:59:52.938192	2016-06-24 13:33:12.506066
62	58	-1599	-1500	8ad6fc	2016-06-22 20:21:13.814121	2016-06-24 13:33:12.506066
61	58	-1499	-1475	8ad6fc	2016-06-22 20:21:13.814121	2016-06-24 13:33:12.506066
59	47	-1474	-1425	6fc9f6	2016-06-22 18:59:52.938192	2016-06-24 13:33:12.506066
64	59	-1474	-1450	8ad6fc	2016-06-22 20:21:13.814121	2016-06-24 13:33:12.506066
63	59	-1449	-1425	8ad6fc	2016-06-22 20:21:13.814121	2016-06-24 13:33:12.506066
60	47	-1424	-1350	6fc9f6	2016-06-22 18:59:52.938192	2016-06-24 13:33:12.506066
65	60	-1424	-1400	8ad6fc	2016-06-22 20:21:13.814121	2016-06-24 13:33:12.506066
168	166	-974	-950	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
169	166	-949	-925	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
170	166	-924	-900	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
171	162	-899	-800	85a0f5	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
172	171	-899	-875	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
173	171	-874	-850	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
174	171	-849	-825	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
175	171	-824	-800	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
176	162	-799	-700	85a0f5	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
177	176	-799	-775	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
178	176	-774	-750	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
179	176	-749	-725	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
180	176	-724	-700	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
181	162	-699	-600	85a0f5	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
182	181	-699	-675	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
183	181	-674	-650	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
184	181	-649	-625	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
185	181	-624	-600	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
186	162	-599	-500	85a0f5	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
187	186	-599	-575	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
188	186	-574	-550	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
189	186	-549	-525	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
190	186	-524	-500	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
163	161	-499	-325	7294f6	2016-06-24 11:57:42.496278	2016-06-24 16:38:12.741683
191	163	-499	-400	85a0f5	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
192	191	-499	-475	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
193	191	-474	-450	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
194	191	-449	-425	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
195	191	-424	-400	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
196	163	-399	-325	85a0f5	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
197	196	-399	-375	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
198	196	-374	-350	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
199	196	-349	-325	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
164	161	-324	-25	7294f6	2016-06-24 11:57:42.496278	2016-06-24 16:38:12.741683
200	164	-324	-300	85a0f5	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
201	164	-299	-200	85a0f5	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
202	201	-299	-275	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
203	201	-274	-250	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
204	201	-249	-225	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
205	201	-224	-200	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
206	164	-199	-100	85a0f5	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
207	206	-199	-175	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
208	206	-174	-150	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
209	206	-149	-125	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
210	206	-124	-100	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
211	164	-99	-25	85a0f5	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
212	211	-99	-75	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
213	211	-74	-50	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
214	211	-49	-25	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
37	0	-6499	2016		2016-06-22 17:37:25.915534	2016-06-24 13:33:12.506066
107	97	14	36	6edb94	2016-06-23 07:54:25.931737	2016-06-24 13:33:12.506066
98	41	37	259	4dc276	2016-06-23 07:28:51.779519	2016-06-24 13:33:12.506066
109	98	37	95	6edb94	2016-06-23 16:42:11.768027	2016-06-24 13:33:12.506066
120	109	37	40	8aedae	2016-06-23 17:10:05.854483	2016-06-24 13:33:12.506066
121	109	41	53	8aedae	2016-06-23 17:10:05.854483	2016-06-24 13:33:12.506066
122	109	54	67	8aedae	2016-06-23 17:10:05.854483	2016-06-24 13:33:12.506066
123	109	68	69	8aedae	2016-06-23 17:10:05.854483	2016-06-24 13:33:12.506066
124	109	70	78	8aedae	2016-06-23 17:10:05.854483	2016-06-24 13:33:12.506066
125	109	79	80	8aedae	2016-06-23 17:10:05.854483	2016-06-24 13:33:12.506066
126	109	81	95	8aedae	2016-06-23 17:10:05.854483	2016-06-24 13:33:12.506066
110	98	96	259	6edb94	2016-06-23 16:42:11.768027	2016-06-24 13:33:12.506066
127	110	96	97	8aedae	2016-06-23 18:14:25.346342	2016-06-24 13:33:12.506066
128	110	98	116	8aedae	2016-06-23 18:14:25.346342	2016-06-24 13:33:12.506066
129	110	117	137	8aedae	2016-06-23 18:14:25.346342	2016-06-24 13:33:12.506066
130	110	138	160	8aedae	2016-06-23 18:14:25.346342	2016-06-24 13:33:12.506066
131	110	161	168	8aedae	2016-06-23 18:14:25.346342	2016-06-24 13:33:12.506066
132	110	169	179	8aedae	2016-06-23 18:14:25.346342	2016-06-24 13:33:12.506066
133	110	180	192	8aedae	2016-06-23 18:14:25.346342	2016-06-24 13:33:12.506066
134	110	193	194	8aedae	2016-06-23 18:14:25.346342	2016-06-24 13:33:12.506066
135	110	195	210	8aedae	2016-06-23 18:14:25.346342	2016-06-24 13:33:12.506066
136	110	211	216	8aedae	2016-06-23 18:14:25.346342	2016-06-24 13:33:12.506066
137	110	217	218	8aedae	2016-06-23 18:14:25.346342	2016-06-24 13:33:12.506066
138	110	219	221	8aedae	2016-06-23 18:14:25.346342	2016-06-24 13:33:12.506066
139	110	222	234	8aedae	2016-06-23 18:14:25.346342	2016-06-24 13:33:12.506066
140	110	235	237	8aedae	2016-06-23 18:14:25.346342	2016-06-24 13:33:12.506066
141	110	238	243	8aedae	2016-06-23 18:14:25.346342	2016-06-24 13:33:12.506066
142	110	244	248	8aedae	2016-06-23 18:14:25.346342	2016-06-24 13:33:12.506066
143	110	249	250	8aedae	2016-06-23 18:14:25.346342	2016-06-24 13:33:12.506066
144	110	251	252	8aedae	2016-06-23 18:14:25.346342	2016-06-24 13:33:12.506066
145	110	253	259	8aedae	2016-06-23 18:14:25.346342	2016-06-24 13:33:12.506066
99	41	260	449	4dc276	2016-06-23 07:28:51.779519	2016-06-24 13:33:12.506066
111	99	260	299	6edb94	2016-06-23 16:42:11.768027	2016-06-24 13:33:12.506066
112	99	300	399	6edb94	2016-06-23 16:42:11.768027	2016-06-24 13:33:12.506066
113	99	400	449	6edb94	2016-06-23 16:42:11.768027	2016-06-24 13:33:12.506066
42	37	450	799	8cb042	2016-06-22 18:36:35.780087	2016-06-24 13:33:12.506066
100	42	450	599	a0c64c	2016-06-23 07:28:51.779519	2016-06-24 13:33:12.506066
114	100	450	519	a5d73e	2016-06-23 16:44:16.966952	2016-06-24 13:33:12.506066
115	100	520	549	a5d73e	2016-06-23 16:44:16.966952	2016-06-24 13:33:12.506066
116	100	550	599	a5d73e	2016-06-23 16:44:16.966952	2016-06-24 13:33:12.506066
101	42	600	719	a0c64c	2016-06-23 07:28:51.779519	2016-06-24 13:33:12.506066
117	101	600	629	a5d73e	2016-06-23 16:46:30.76206	2016-06-24 13:33:12.506066
118	101	630	679	a5d73e	2016-06-23 16:46:30.76206	2016-06-24 13:33:12.506066
119	101	680	719	a5d73e	2016-06-23 16:46:30.76206	2016-06-24 13:33:12.506066
102	42	720	799	a0c64c	2016-06-23 07:28:51.779519	2016-06-24 13:33:12.506066
43	37	800	1491	f39306	2016-06-22 18:36:35.780087	2016-06-24 13:33:12.506066
103	43	800	999	f1ac06	2016-06-23 07:28:51.779519	2016-06-24 13:33:12.506066
104	43	1000	1199	f1ac06	2016-06-23 07:28:51.779519	2016-06-24 13:33:12.506066
105	43	1200	1491	f1ac06	2016-06-23 07:28:51.779519	2016-06-24 13:33:12.506066
44	37	1492	1789	f84c48	2016-06-22 18:36:35.780087	2016-06-24 13:33:12.506066
45	37	1790	2016	815a4e	2016-06-22 18:36:35.780087	2016-06-24 13:33:12.506066
160	0	-999	14		2016-06-24 11:57:42.496278	2016-06-24 16:38:12.741683
161	160	-999	14	5882ff	2016-06-24 11:57:42.496278	2016-06-24 16:38:12.741683
216	165	2	14	85a0f5	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
165	161	-24	14	7294f6	2016-06-24 11:57:42.496278	2016-06-24 16:38:12.741683
215	165	-24	1	85a0f5	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
162	161	-999	-500	7294f6	2016-06-24 11:57:42.496278	2016-06-24 16:38:12.741683
166	162	-999	-900	85a0f5	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
167	166	-999	-975	a2b6ef	2016-06-24 12:50:06.143018	2016-06-24 16:38:12.741683
\.


--
-- Name: chronology_id_seq; Type: SEQUENCE SET; Schema: public; Owner: arkeogis
--

SELECT pg_catalog.setval('chronology_id_seq', 275, true);


--
-- Data for Name: chronology_root; Type: TABLE DATA; Schema: public; Owner: arkeogis
--

COPY chronology_root (root_chronology_id, admin_group_id, author_user_id, credits, active, geom) FROM stdin;
261	26	63	ArkeoGIS 2015	t	0103000020E6100000010000000D00000000000000800F3640C89CDAC669EF40400000000000C93840AA8119C8E86344400000000000883D40C37A166C9FA9454000000000002943400188486793634640000000008093494029F7491BFDB2474000000000809C5040938AB86D944B464000000000A0B55340E0DD5F3F4ACA454000000000C0FE5340B6877483488C43400000000020535440D404F009C07E404000000000809C50403F3126F5E60F334000000000C0FB464000D4580D639723400000000080393940FBAC592E8BDC354000000000800F3640C89CDAC669EF4040
274	27	74	ArkeoGIS 2016	f	0103000020E6100000010000000E0000000000000000EB14C0938AB86D944B464000000000003CFEBF6BC70D3FF9B945400000000000120B40EBDC27CA134F45400000000000FE0F40D0960842ABB844400000000000831340F68A45E8CD7A4340000000000080A63F138DD70D967942400000000000A40FC0916B011AAEE841400000000000F31BC0CAAFF3BBFCDA414000000000001825C00B9AE5AB1A0D424000000000008026C0912BA8C8BE57434000000000007225C0F1578EE7625B444000000000001825C08FD520587C5B454000000000002923C0E1BC62A3954F46400000000000EB14C0938AB86D944B4640
37	15	61	remerciements à S.Fichtl et B.Girard pour leurs contributions	t	0103000020E6100000010000000E000000000000008023214021F9F64ED7BE464000000000004515406F431619863746400000000000C60B40324FC842F3B6464000000000000A0440647B9F9D664B47400000000000CC0540162FB4601D0848400000000000FE0F4099C6E24A0B1C49400000000080331F40B46EFFE51CBA494000000000C047264060F0B389D588494000000000C08D2B4057AC7277E3EE48400000000040C02D409B1FDD25A2DE474000000000007D294044C33391EC9F474000000000805828409697A4DD49304740000000000026264072AE42313EE24640000000008023214021F9F64ED7BE4640
160	24	62	Sophie Bouffier	t	0103000020E6100000010000000500000000000000806F30C0FF1E2FBE2925334000000000806F30C09D82F20B382C49400000000000204C409D82F20B382C49400000000000204C40FF1E2FBE2925334000000000806F30C0FF1E2FBE29253340
227	25	62	Py 1993	t	0103000020E6100000010000000500000000000000407211406F6BECE20F6E45400000000040721140B037623E165346400000000000F01E40B037623E165346400000000000F01E406F6BECE20F6E454000000000407211406F6BECE20F6E4540
\.


--
-- Data for Name: chronology_tr; Type: TABLE DATA; Schema: public; Owner: arkeogis
--

COPY chronology_tr (lang_isocode, chronology_id, name, description) FROM stdin;
D 	0	No chronology	This chronology does not exists
fr	261	Proche-Orient entre 14000 et 4500 av. JC.	Chronologie établie par la Maison de l'Orient et de la Méditerranée pour l'Atlas des Sites du Proche-Orient (ASPRO ), entre 14000 et 4500 av. JC
en	261	Near East between 14000 and 4500 BC	ASPRO chronology is a dating system of the ancient Near East used by the Maison de l'Orient et de la Méditerranée for archaeological sites aged between 14000 and 4500 BC.
en	262	Epipaleolithic	
fr	262	Epipaléolithique	
fr	264	Kébarien à géométriques, Moschabien - Zarzien	
en	264	Geometric Kebaran, Moschabian - Zarzian	
en	265	Natufian – Late Zarzian	
fr	265	Natoufien - Zarzien fina	
en	263	Neolithic	
fr	263	Néolithique	
en	266	Proto-Neolithic, Pre-Pottery Neolithic A (PPNA) -Khiamian-Sultanian-Harifian	
fr	266	Protonéolithique, Pre-Pottery Neolithic A (PPNA) - Khiamien - Sultanien - Harifien	
en	267	Early and Middle Pre-Pottery Neolithic B (PPNB)	
fr	267	Pre-Pottery Neolithic B (PPNB) ancien et moyen	
en	268	Late Pre-Pottery Neolithic B (PPNB)	
fr	268	Pre-Pottery Neolithic B (PPNB) récent	
en	269	Dark Faced Burnished Ware (DFBW) - Çatal Hüyük - Umm Dabaghiyah - Sotto – Ubaid 0	
fr	269	Dark Faced Burnished Ware (DFBW) - Çatal Hüyük - Umm Dabaghiyah - Sotto - Obeid 0	
en	270	Hassuna - Samarra - Halaf – Ubaid 1	
fr	270	Hassuna - Samarra - Halaf - Obeid 1	
en	271	Pottery Neolithic A (PNA) -  Late Halaf – Ubaid 2	
fr	271	Pottery Neolithic A (PNA) - Halaf final - Obeid 2	
en	272	Pottery Neolithic B (PNB) – Ubaid 3	
fr	272	Pottery Neolithic B (PNB) - Obeid 3	
en	273	Ubaid 4	
fr	273	Obeid 4	
en	227	Iron Age in the Provence	Iron Age in the Provence
fr	227	La Provence de l'âge du Fer	Chronologie de l'âge du Fer en Provence
en	228	Iron Age	
fr	228	âge du Fer	
fr	229	Transition Bronze-Fer	
en	229	Transition Bronze / Fer	
en	238	fourth quarter of the eigthth century BC	
fr	238	4e quart du VIIIe siècle av. J.-C.	
fr	239	1er quart du VIIe siècle av. J.-C.	
en	239	first quarter of the seventh century BC	
en	230	Fer I	
fr	230	Fer I	
en	231	Fer I ancien	
fr	231	Fer I ancien	
en	240	second quarter of the seventh century BC	
fr	240	2e quart du VIIe siècle av. J.-C.	
fr	160	La méditerranée du Premier millénaire avant JC	Chronologie méditerranéenne du Premier millénaire avant JC
en	160	The first millennium BC in the mediterranea	Chronologie of the first millennium BC in the mediterranea
en	161	First millennium BC in the mediterranean	
fr	161	Premier millénaire avant JC.	
en	162	Pre-classical	
fr	162	Préclassique	
en	166	tenth century BC	
fr	166	Xe siècle av. J.-C.	
fr	167	1er quart du Xe siècle av. J.-C.	
en	167	first quarter of the tenth century BC	
fr	168	2e quart du Xe siècle av. J.-C.	
en	168	second quarter of the tenth century BC	
en	169	third quarter of the tenth century BC	
fr	169	3e quart du Xe siècle av. J.-C.	
en	170	fourth quarter of the tenth century BC	
fr	170	4e quart du Xe siècle av. J.-C.	
en	171	ninth century BC	
fr	171	IXe siècle av. J.-C.	
en	172	first quarter of the ninth century BC	
fr	172	1er quart du IXe siècle av. J.-C.	
en	173	second quarter of the ninth century BC	
fr	173	2e quart du IXe siècle av. J.-C.	
en	174	third quarter of the ninth century BC	
fr	174	3e quart du IXe siècle av. J.-C.	
en	175	fourth quarter of the ninth century BC	
fr	175	4e quart du IXe siècle av. J.-C.	
en	176	eigthth century BC	
fr	176	VIIIe siècle av. J.-C.	
fr	177	1er quart du VIIIe siècle av. J.-C.	
en	177	first quarter of the eigthth century BC	
en	178	second quarter of the eigthth century BC	
fr	178	2e quart du VIIIe siècle av. J.-C.	
en	179	third quarter of the eigthth century BC	
fr	179	3e quart du VIIIe siècle av. J.-C.	
en	180	fourth quarter of the eigthth century BC	
fr	180	4e quart du VIIIe siècle av. J.-C.	
en	181	seventh century BC	
fr	181	VIIe siècle av. J.-C.	
en	182	first quarter of the seventh century BC	
fr	182	1er quart du VIIe siècle av. J.-C.	
en	183	second quarter of the seventh century BC	
fr	183	2e quart du VIIe siècle av. J.-C.	
fr	184	3e quart du VIIe siècle av. J.-C.	
en	184	third quarter of the seventh century BC	
en	185	fourth quarter of the seventh century BC	
fr	185	4e quart du VIIe siècle av. J.-C.	
en	186	sixth century BC	
fr	186	VIe siècle av. J.-C.	
en	187	first quarter of the sixth century BC	
fr	187	1er quart du VIe siècle av. J.-C.	
fr	188	2e quart du VIe siècle av. J.-C.	
en	188	second quarter of the sixth century BC	
fr	189	3e quart du VIe siècle av. J.-C.	
en	189	third quarter of the sixth century BC	
en	190	fourth quarter of the sixth century BC	
fr	190	4e quart du VIe siècle av. J.-C.	
en	163	Classical	
fr	163	Classique	
fr	191	Ve siècle av. J.-C.	
en	191	fifth century BC	
en	192	first quarter of the fifth century BC	
fr	192	1er quart du Ve siècle av. J.-C.	
en	193	second quarter of the fifth century BC	
fr	193	2e quart du Ve siècle av. J.-C.	
en	194	third quarter of the fifth century BC	
fr	194	3e quart du Ve siècle av. J.-C.	
fr	195	4e quart du Ve siècle av. J.-C.	
en	195	fourth quarter of the fifth century BC	
fr	196	3 premiers ¼ du IVe siècle av. J.-C.	
en	196	three first quarter of the fourth century BC	
en	197	first quarter of the fourth century BC	
fr	197	1er quart du IVe siècle av. J.-C.	
fr	198	2e quart du IVe siècle av. J.-C.	
en	198	second quarter of the fourth century BC	
en	199	third quarter of the fourth century BC	
fr	199	3e quart du IVe siècle av. J.-C.	
en	164	Hellenistic	
fr	164	Hellénistique	
en	200	last quarter of the fourth century BC	
fr	200	4e quart du IVe siècle av. J.-C.	
en	201	third century BC	
fr	201	IIIe siècle av. J.-C.	
fr	202	1er quart du IIIe siècle av. J.-C.	
en	202	first quarter of the third century BC	
en	203	second quarter of the third century BC	
fr	203	2e quart du IIIe siècle av. J.-C.	
en	204	third quarter of the third century BC	
fr	204	3e quart du IIIe siècle av. J.-C.	
en	205	fourth quarter of the third century BC	
fr	205	4e quart du IIIe siècle av. J.-C.	
en	206	second century BC	
fr	206	IIe siècle av. J.-C.	
en	207	first quarter of the second century BC	
fr	207	1er quart du IIe siècle av. J.-C.	
en	208	second quarter of the second century BC	
fr	208	2e quart du IIe siècle av. J.-C.	
en	209	third quarter of the second century BC	
fr	209	3e  quart du IIe siècle av. J.-C.	
en	210	fourth quarter of the second century BC	
fr	210	4e quart du IIe siècle av. J.-C.	
en	211	three first quarters of the first century BC	
fr	211	3 premiers ¼ du Ier siècle av. J.-C.	
en	212	first quarter of the first century BC	
fr	212	1er quart du Ier siècle av. J.-C.	
en	213	second quarter of the first century BC	
fr	213	2e quart du Ier siècle av. J.-C.	
en	214	third quarter of the first century BC	
fr	214	3e quart du Ier siècle av. J.-C.	
en	165	Principate	
fr	165	Principat	
en	215	last quarter of the first century BC	
fr	215	4e  quart du Ier siècle av. J.-C.	
en	216	End of the principate	
fr	216	Fin du principat	
en	241	third quarter of the seventh century BC	
fr	241	3e quart du VIIe siècle av. J.-C.	
fr	232	Fer I récent	
en	232	Fer I récent	
en	242	fourth quarter of the seventh century BC	
fr	242	4e quart du VIIe siècle av. J.-C.	
en	243	first quarter of the sixth century BC	
fr	243	1er quart du VIe siècle av. J.-C.	
fr	244	2e quart du VIIe siècle av. J.-C.	
en	244	second quarter of the sixth century BC	
en	245	third quarter of the sixth century BC	
fr	245	3e quart du VIIe siècle av. J.-C.	
en	233	Transition Fer I - Fer II	
fr	233	Transition Fer I-Fer II	
fr	246	4e quart du VIe siècle av. J.-C.	
en	246	fourth quarter of the sixth century BC	
en	247	first quarter of the fifth century BC	
fr	247	1er quart du Ve siècle av. J.-C.	
en	248	second quarter of the fifth century BC	
fr	248	2e quart du Ve siècle av. J.-C.	
en	249	third quarter of the fifth century BC	
fr	249	3e quart du Ve siècle av. J.-C.	
en	234	Fer II	
fr	234	Fer II	
en	235	Fer II ancien	
fr	235	Fer II ancien	
en	250	fourth quarter of the fifth century BC	
fr	250	4e quart du Ve siècle av. J.-C.	
en	251	first quarter of the fourth century BC	
fr	251	1er quart du IVe siècle av. J.-C.	
en	252	second quarter of the fourth century BC	
fr	252	2e quart du IVe siècle av. J.-C.	
en	253	third quarter of the fourth century BC	
fr	253	3e quart du IVe siècle av. J.-C.	
en	254	last quarter of the fourth century BC	
fr	254	4e quart du IVe siècle av. J.-C.	
fr	236	Fer II récent	
en	236	Fer II récent	
en	255	first quarter of the third century BC	
fr	255	1er quart du IIIe siècle av. J.-C.	
fr	256	2e quart du IIIe siècle av. J.-C.	
en	256	second quarter of the third century BC	
en	257	third quarter of the third century BC	
fr	257	3e quart du IIIe siècle av. J.-C.	
fr	258	4e quart du IIIe siècle av. J.-C.	
en	258	fourth quarter of the third century BC	
en	259	first quarter of the second century BC	
fr	259	1er quart du IIe siècle av. J.-C.	
fr	260	2e quart du IIe siècle av. J.-C. à -119	
en	260	second quarter of the second century BC	
en	237	Gallo-Roman	
fr	237	Gallo-Romain	
en	274	Iron Age in the Iberian Peninsula	Iron Age in the Iberian Peninsula
es	274	La Edad de Hierro en Península Ibérica	La Edad de Hierro en Península Ibérica
fr	274	Péninsule Ibérique de l'âge du Fer	L'âge du Fer en Péninsule Ibériqu
en	275	Iron age	
fr	275	Age du Fer	
es	275	Edad del Hierro	
en	37	Continental Europe from the Neolithic to nowadays	Continental Europe from the Neolithic to nowadays.
fr	37	Europe Continental depuis le Néolithique à nos jours	Première des chronologies réalisées pour ArkeoGIS.
fr	38	Néolithique	
en	38	Neolithic	
en	39	Bronze age	
fr	39	Bronze	
en	46	Early bronze age	
fr	46	Bronze ancien	
en	49	BRA1	
fr	49	BRA1	
en	52	BRA1a	
fr	52	BRA1a	
en	53	BRA1b	
fr	53	BRA1b	
en	50	BRA2	
fr	50	BRA2	
fr	54	BRA2a	
en	54	BRA2a	
en	55	BRA2b	
fr	55	BRA2b	
en	51	BRA3	
fr	51	BRA3	
en	56	BRA3a	
fr	56	BRA3a	
en	57	BRA3b	
fr	57	BRA3b	
en	47	Middle bronze age	
fr	47	Bronze moyen	
fr	58	BRM1	
en	58	BRM1	
en	62	BRM1b	
fr	62	BRM1b	
fr	61	BRM1a	
en	61	BRM1a	
fr	59	BRM2	
en	59	BRM2	
en	64	BRM2b	
fr	64	BRM2a	
en	63	BRM2a	
fr	63	BRM2b	
en	60	BRM3	
fr	60	BRM3	
en	65	BRM3a	
fr	65	BRM3a	
fr	66	BRM3b	
en	66	BRM3b	
en	48	Late bronze age	
fr	48	Bronze final	
fr	69	BRF1	
en	69	BRF1	
en	70	BRF1a	
fr	70	BRF1a	
en	71	BRF1b	
fr	71	BRF1b	
en	68	BRF2	
fr	68	BRF2	
en	72	BRF2a	
fr	72	BRF2a	
fr	73	BRF2b	
en	73	BRF2b	
fr	67	BRF3	
en	67	BRF3	
en	74	BRF3a	
fr	74	BRF3a	
en	75	BRF3b	
fr	75	BRF3b	
en	40	Iron age	
fr	40	Fer	
en	76	Hallstatt C	
fr	76	Hallstatt C	
en	82	HAC1	
fr	82	HAC1	
en	87	HAC1a	
fr	87	HAC1a	
en	88	HAC1b	
fr	88	HAC1b	
en	83	HAC2	
fr	83	HAC2	
en	146	HAC2a	
fr	146	HAC2a	
en	147	HAC2b	
fr	147	HAC2b	
fr	77	Hallstatt D	
en	77	Hallstatt D	
en	84	HAD1	
fr	84	HAD1	
en	148	HAD1a	
fr	148	HAD1a	
fr	149	HAD1b	
en	149	HAD1b	
en	85	HAD2	
fr	85	HAD2	
en	150	HAD2a	
fr	150	HAD2a	
en	151	HAD2b	
fr	151	HAD2b	
fr	86	HAD3	
en	86	HAD3	
en	152	HAD3a	
fr	152	HAD3a	
en	153	HAD3b	
fr	153	HAD3b	
en	78	La Tène A	
fr	78	La Tène A	
fr	89	LTA1	
en	89	LTA1	
en	154	LTA1a	
fr	154	LTA1a	
en	155	LTA1b	
fr	155	LTA1b	
en	90	LTA2	
fr	90	LTA2	
en	156	LTA2a	
fr	156	LTA2a	
en	157	LTA2b	
fr	157	LTA2b	
en	79	La Tène B	
fr	79	La Tène B	
en	91	LTB1	
fr	91	LTB1	
en	158	LTB1a	
fr	158	LTB1a	
en	159	LTB1b	
fr	159	LTB1b	
fr	92	LTB2	
en	92	LTB2	
en	217	LTB2a	
fr	217	LTB2a	
en	218	LTB2b	
fr	218	LTB2b	
en	80	La Tène C	
fr	80	La Tène C	
fr	93	LTC1	
en	93	LTC1	
en	219	LTC1a	
fr	219	LTC1a	
en	220	LTC1b	
fr	220	LTC1b	
fr	94	LTC2	
en	94	LTC2	
en	221	LTC2a	
fr	221	LTC2a	
en	222	LTC2b	
fr	222	LTC2b	
en	81	La Tène D	
fr	81	La Tène D	
fr	95	LTD1	
en	95	LTD1	
fr	223	LTD1a	
en	223	LTD1a	
en	224	LTD1b	
fr	224	LTD1b	
en	96	LTD2	
fr	96	LTD2	
en	225	LTD2a	
fr	225	LTD2a	
en	226	LTD2b	
fr	226	LTD2b	
en	41	Roman Empire	
fr	41	Empire romain	
en	97	Augstean period	
fr	97	Période augustéenne	
fr	106	Auguste	
en	106	Augustus	
en	107	Tiberius	
fr	107	Tibère	
en	98	Early Empire	
fr	98	Haut-Empire	
en	109	Julio-Claudians / Flavians	
fr	109	Julio-Claudiens	
fr	120	Caligula	
en	120	Calígula	
en	121	Claudius	
fr	121	Claude	
en	122	Nero	
fr	122	Néron	
en	123	Galba/Othon/Vitelius	
fr	123	Galba	
en	124	Vespasian	
fr	124	Vespasien	
en	125	Titus	
fr	125	Titus	
fr	126	Domitien	
en	126	Domitian	
en	110	Antonine Dynasty	
fr	110	Antonin Sévères	
en	127	Nerva	
fr	127	Nerva	
en	128	Trajan	
fr	128	Trajan	
fr	129	Hadrien	
en	129	Hadrian	
en	130	Antoninus Pius	
fr	130	Antonin le Pieux	
en	131	Lucius Verus	
fr	131	Lucius Verus	
en	132	Marcus Aurelius	
fr	132	Marc-Aurèle	
en	133	Commodus	
fr	133	Commode	
en	134	Clodius Albinu	
fr	134	Clodius Albinus	
en	135	Septimius Severus	
fr	135	Septime Sévère	
en	136	Caracalla	
fr	136	Caracalla	
en	137	Gela/Macrinus	
fr	137	Gela/Macrin	
en	138	Elagabalus	
fr	138	Elagabale	
en	139	Severus Alexander	
fr	139	Sévère Alexandre	
en	140	Maximinus Thrax	
fr	140	Maximin Ier le Thrace	
fr	141	Gordien III	
en	141	Gordian III	
fr	142	Philippe l'Arabe	
en	142	Philip the Arab	
en	143	Decius	
fr	143	Dèce	
fr	144	Trébonien Galle	
en	144	Trebonianus Gallus	
en	145	Valerian	
fr	145	Valérien (Orient)	
fr	99	Antiquité tardive	
en	99	Late Antiquity	
en	111	Constantine	
fr	111	Constantin	
en	112	4th Century	
fr	112	IVè siècle	
en	113	Early half 5th Century	
fr	113	1ere moitié Vè siècle	
fr	42	Mérovingien	
en	42	Merovingian	
en	100	Early merovingian	
fr	100	Mérovingien ancien	
en	114	Early merovingian I	
fr	114	Mérovingien ancien I	
en	115	Early merovingian II	
fr	115	Mérovingien ancien II	
fr	116	Mérovingien ancien III	
en	116	Early merovingian III	
en	101	Late merovingian	
fr	101	Mérovingien récent	
en	117	Late merovingian I	
fr	117	Mérovingien récent I	
en	118	Late merovingian II	
fr	118	Mérovingien récent II	
en	119	Late merovingian III	
fr	119	Mérovingien récent III	
en	102	Caroligian	
fr	102	Caroligien	
en	43	Middle age	
fr	43	Moyen-Âge	
en	103	Middle age I	
fr	103	Moyen-Âge I	
en	104	Middle age II	
fr	104	Moyen-Âge II	
en	105	Classic Middle age	
fr	105	Moyen-Âge classique	
fr	44	Moderne	
en	44	Modern times	
en	45	Contemporary times	
fr	45	Contemporaine	
\.


--
-- PostgreSQL database dump complete
--

