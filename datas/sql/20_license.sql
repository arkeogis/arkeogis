INSERT INTO "license" ("id", "name", "url") VALUES (0, '-', 'http://www.arkeogis.org/choose_a_license');
INSERT INTO "license" ("id", "name", "url") VALUES (1, 'CC-BY-NC', 'http://creativecommons.org/licenses/by-nc/4.0/');
INSERT INTO "license" ("id", "name", "url") VALUES (2, 'CC-BY-NC-SA', 'http://creativecommons.org/licenses/by-nc-sa/4.0/');
