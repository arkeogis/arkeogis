CREATE USER user_arkeogis with SUPERUSER CREATEDB PASSWORD 'arkeogis';
CREATE EXTENSION postgis;
CREATE EXTENSION unaccent;
CREATE DATABASE arkeogis with OWNER user_arkeogis;