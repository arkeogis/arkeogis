package main

import (
	arkeogis "gitlab.huma-num.fr/arkeogis/arkeogis-server"
)

func main() {
	arkeogis.Start()
}
