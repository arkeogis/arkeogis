# Language Translating

Ce document détail la façon dont doit être écrit le code de Arkeogis de façon à être traduit dans diverses langues

## Généralités

Le code (HTML/JS/GO/...) doit contenir des mots clefs de la forme :
SECTION.CHAMP.T_MESSAGE_CONTEXT

Exemples :
USER.HEADER_ADDUSER.T_TITLE
USER.FIELD_EMAIL.T_TITLE
USER.FIELD_EMAIL.T_CHECK_MANDATORY
...

Ce mot clef est composé de 3 éléments séparé par des points. Tout doit être en majuscule, et le dérnier élément doit commencer par "T_"

## Code HTML

### Voici quelques exemples :

Label d'un champ :
```html
<label translate>USER.FIELD_EMAIL.LABEL</label>
```

Les erreurs (checks) du champ :
```html
<div ng-message-exp="['required']" ng-if="userForm.email.$dirty">{{ 'USER.FIELD_EMAIL.T_CHECK_MANDATORY' | translate }}</div>
<div ng-message-exp="['minlength', 'maxlength', 'pattern']">{{ 'USER.FIELD_EMAIL.T_CHECK_INCORRECT' | translate }}</div>
```

## Code JS

todo

## Code Go

todo

## Arborescence

Voici comment doivent être écrirs chacun des trois éléments qui composent le mot clef :
 -  Le premier élément représente la section, exemple : "USER", "IMPORT", "MAP", etc.
 -  Le deuxième élément représente l'élement visuel, exemple "HEADER_ADDUSER", "FIELD_EMAIL", "BUTTON_OK"
 -  Le troisième élément représente les différents message lié à l'élement visuel, exemple : "T_CHECK_MANDATORY", "T_CHECK_INCORRECT", "T_DESCRIPTION", "T_TITLE", etc.

### Tentative d'homogénisation des éléments

Niveau 1 :
- USER
- IMPORT
- MAP
-
Niveau 2 :
- HEADER_* pour un entête
- FOOTER_* ...
- FIELD_* pour les champs d'un formulaire (qu'il soit textfield, selects, radios, checkboxs, etc.)
- BUTTON_* pour les boutons (Ok, Cancel, ...)

Niveau 3 : (le plus important)
- T_TITLE pour un titre
- T_DESCRIPTION pour une description
- T_CHECK_* pour les messages lié aux champs d'un formulaire
- T_ITEM_* pour les items d'un champ menu, radio, etc.
