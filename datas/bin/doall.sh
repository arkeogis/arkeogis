#!/bin/bash

function fail() {
    echo "$1"
    exit 1
}

sudo bash <<EOF
cd /tmp && su postgres -c '' &> /dev/null && su postgres -c 'dropdb --if-exists arkeogis && dropuser --if-exists arkeogis && createuser arkeogis && createdb arkeogis -O arkeogis && psql arkeogis -c "CREATE EXTENSION postgis" && psql arkeogis -c "CREATE EXTENSION unaccent"'
EOF

echo "build tools..."
cd ..
GOPATH=$PWD/src/server PKG_CONFIG_PATH=$PWD/src/server/src/github.com/lukeroth/gdal gulp tools || fail "tools build failed"
cd -

cd ../dist
./tools/xmltopsql | psql -U arkeogis arkeogis || fail "xmltopsql failed"

cd -
./bin/import_sql.sh || fail "import_sql.sh"

#echo "Import des geonames..."
#cd ../dist && ./tools/geonames_import || fail "Import des geonames failed"

#echo "Import des caracs..."
#./tools/caracsimport || fail "Import des caracs failed"

echo "done."
