# SQL for Arkeogis v4

Arkeogis V4 use (sqldesigner)[https://sqldesigner.dev.arkeogis.org/] for it's sql schema. When developping, the schema from sqldesigner have to be exported in XML. A Tool developped for arkeogis v4 is then used for converting the schema in Go Objects.

Here is a simple procedure for adding a column, here, it will be "idArkPeriodo" in the "chronology" table.

## Build the required tools

If it was not done already, build the required tools (xmltogo) using :
```shell
gulp tools
```

## Add the column in sqldesigner

Use sqldesigner to add the column like this :

![Add idArkPeriodo](sql/addIdArkPeriodo.png)

## Export XML

In sqldesigner, go to Save/Load, and click on the "SAVE XML" button

![Save XML](sql/savexml.png)

## Copy XML in the repository
Copy the XML content from this window, in the file `src/server/src/gitlab.huma-num.fr/arkeogis/arkeogis-server/db-schema.xml`

## Run the converter tool

```bash
cd dist && ./tools/xmltogo > ../src/server/src/gitlab.huma-num.fr/arkeogis/arkeogis-server/model/model.go
```

## tests...

## Commit the changes

```bash
cd src/server/src/gitlab.huma-num.fr/arkeogis/arkeogis-server && git commit model/model.go db-schema.xml
```

