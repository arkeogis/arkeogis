#!/bin/bash

function distToSrcWeb() {
    local lang=$1
    echo "distToSrcWeb $lang ..."
    mv src/web/languages/$lang.json src/web/languages/$lang.old.json
    cp dist/public/languages/$lang.json src/web/languages/$lang.json
}

function distToSrcServer() {
    local lang=$1
    echo "distToSrcServer $lang ..."
    mv src/server/languages/$lang.json src/server/languages/$lang.old.json
    cp dist/languages/$lang.json src/server/languages/$lang.json
}

for lang in fr en de es; do
    distToSrcWeb $lang;
    distToSrcServer $lang;
done
