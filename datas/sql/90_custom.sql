--
-- custom index on City
-- we create a fonction f_unaccent that is IMMUTABLE, so that we can make an index using unaccent on name
-- inspiration: http://stackoverflow.com/questions/11005036/does-postgresql-support-accent-insensitive-collations
--

CREATE OR REPLACE FUNCTION f_unaccent(text)
  RETURNS text AS
$func$
SELECT unaccent('unaccent', $1)
$func$  LANGUAGE sql IMMUTABLE SET search_path = public, pg_temp;

create index city_tr_name_ascii_varchar_idx on "city_tr" ("name_ascii" varchar_pattern_ops);
create index city_tr_name_unaccent_varchar_idx on "city_tr" (lower(f_unaccent("name")) varchar_pattern_ops);


--
-- this idx function help ordering by a custom array of values
-- inspiration: http://wiki.postgresql.org/wiki/Array_Indexhttp://wiki.postgresql.org/wiki/Array_Index
--
CREATE OR REPLACE FUNCTION idx(anyarray, anyelement)
  RETURNS INT AS
$$
  SELECT i FROM (
     SELECT generate_series(array_lower($1,1),array_upper($1,1))
  ) g(i)
  WHERE $1[i] = $2
  LIMIT 1;
$$ LANGUAGE SQL IMMUTABLE;
