#!/bin/bash

me=$(whoami)

function export_tables() {
    sudo bash <<EOF
echo -n "pg_dump of $1..."
cd /tmp
su postgres -c "pg_dump $2 -a arkeogis > /tmp/arkeo.sql"
chown $me:$me /tmp/arkeo.sql
EOF
    mv /tmp/arkeo.sql sql/$1.sql
    echo " done."
}

#export_tables 20_langs "-t lang"
#export_tables 30_users "-t user -t photo -t user_id_seq -t group -t group_id_seq -t permission -t permission_id_seq -t group__permission -t group_tr -t user__group -t company -t user__company"
#export_tables 40_caracs "-t charac -t charac_id_seq -t charac_tr -t charac_root"
#export_tables 40_chronologies "-t chronology -t chronology_id_seq -t chronology_tr -t chronology_root"
export_tables 50_geonames "-t city -t city_tr -t country -t country_tr -t continent -t continent_geonameid_seq -t continent_tr -t continent_tr_continent_geonameid_seq -t lang -t lang_tr"

exit 0
